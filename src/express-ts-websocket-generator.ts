import { Application } from "express";
import * as expressws from "express-ws";
import * as fs from "fs";
import * as path from "path";
import "reflect-metadata";
import "socket.io";
import { Configuration, ILinterOptions, Linter } from "tslint";
import { v4 as uuidv4 } from "uuid";
import { AngularWebsocketGenerator } from "./generators/angular-websocket-generator";
import { IClientGeneratorSettings } from "./generators/client-generator-settings";
import { GenericWebsocketGenerator } from "./generators/generic-websocket-generator";
import { IWebsocketGenerator } from "./generators/websocket-generator";
import { Utils } from "./utils";

interface IDictionary<T> {
	[key: string]: T;
}


export class ExpressWebsocketGenerator {
    public static debugLogging: Boolean = false;
    /**
     * Converts class instance to Rest router and exposes all methods that have @WebsocketMethod decorators.
     * @param routerClass class instance which we want to transform to router
     */
    public static generateWebsocketServer(express: Application, classInstances: any[]) {
        if (!classInstances.length) {
            throw new Error("No classes specified for websocket server!");
        }

        const ews = expressws(express);

        const router = this.generateWebsocketRouter(classInstances);

        (express as any).ws("/ws", (ws: any, req: Request) => {
            const listeners: any[] = [];

            ws.on("message", (message: any) => {
                try {
                    if(ExpressWebsocketGenerator.debugLogging){
                        console.log("Received message:", message);
                    }

                    const json = JSON.parse(message);
                    const messageType = json.type;
                    const data = json.data;
                    const id = json.id;
                    // Call a specific method on the class and return result
                    if (router[messageType]) {
                        router[messageType](ws,req, id, data);
                    } else if (messageType.indexOf("event-subscribe:") === 0) {
                        const eventPath = messageType.split(":")[1];
                        const listener = this.addWebsocketListener(classInstances, eventPath, ws);
                        if (listener) {
                            listeners.push(listener);
                        }
                    } else if (messageType.indexOf("event-unsubscribe:") === 0) {
                        const eventPath = messageType.split(":")[1];
                        let i = 0;
                        while (i < listeners.length) {
                            const listener = listeners[i];
                            var isRemoved = this.removeWebsocketListener(classInstances, eventPath, listener);
                            if(isRemoved){
                                listeners.splice(i,1);
                            }else{
                                i++;
                            }
                        }
                    } else {
                        console.error(`Websocket handler for message type:${messageType} does not exist.`);
                    }
                } catch (e) {
                    console.error(`Websocket error:`, e);
                }
            });
            ws.on("error", (error: any) => {
                console.error("Websocket error:", error);
            });

            ws.on("close", (error: any) => {
                if(ExpressWebsocketGenerator.debugLogging){
                    console.log("Websocket connection closed:",error);
                }
                this.removeAllWebsocketListeners(classInstances, listeners);
            });
            // ws.send("{ 'type' : 'ping' }");
        });
    }

    public static generateClientServiceFromFile(tsClassFilePath: string, outputFile: string, generatorType: "default" | "angular" = "default", settings?: IClientGeneratorSettings) {
        let generator: IWebsocketGenerator;
        if (generatorType === "default") {
            generator = new GenericWebsocketGenerator();
        } else if (generatorType === "angular") {
            generator = new AngularWebsocketGenerator();
        }

        if (!generator) {
            throw new Error("cannot find generator for type:" + generatorType);
        }

        this.generateClientService(tsClassFilePath, outputFile, generator, settings);
    }
    
    public static generateClientService(tsClassFilePath: string, outputFile: string, generator: IWebsocketGenerator, settings?: IClientGeneratorSettings) {
        const generatorOutput = generator.generateClientServiceFromFile(tsClassFilePath, outputFile, settings);

        const outputDir = path.dirname(outputFile);
        if (!fs.existsSync(outputDir)) {
            fs.mkdirSync(outputDir, {recursive : true});
        }

        fs.writeFileSync(outputFile,  generatorOutput);

        // this.lintCode(outputFile);
    }

    private static generateWebsocketRouter(classInstances: any[]): IDictionary<(ws: WebSocket, req: Request, messageId: string, data: any) => void> {
        const router: IDictionary<(ws: WebSocket, req: Request, messageId: string, data: any) => void> = {};

        for (const classInstance of classInstances) {
            const apiPrefix = classInstance.constructor.name;

            for (const methodName in classInstance) {

                const method = classInstance[methodName] as any;

                if (typeof(method) === "function") {
                    // Try to determine request method type form method name

                    // Get all method types in a string[]
                    const methodMetadata = Reflect.getMetadata("design:paramtypes", classInstance, methodName);
                    if (!methodMetadata) {
                        console.warn(`${apiPrefix}.${methodName} does not contain reflection metadata.`);
                        continue;
                    }
                    const fnArgTypes = methodMetadata.map((x: any) => this.getStringTypeFromReflectionType(x));
                    const fnArgs = Utils.getFunctionArgs(method);

                    if (fnArgTypes.length !== fnArgs.length) {
                        throw new Error("Length of method types and argument names does not match!");
                    }

                    // Create a router method
                    const messageType = Utils.getEndpointPath(methodName, apiPrefix);
                    (router as any)[messageType] = async (ws: WebSocket, request: Request,  messageId: string, data: any) => {
                        if(data === undefined){
                            data = {};
                        }
                        try {
                            // Get query parameters from method parameters and pass them on
                            const args: any[] = [];
                            for (let i = 0; i < fnArgs.length; i++) {
                                const paramName = fnArgs[i];

                                const fnParamType = fnArgTypes[i];
                                let reqParamValue = data[paramName];
                                const reqParamType = typeof(reqParamValue);
                                // Check if we need to convert parameter to a different type since query params are always strings
                                if (fnParamType !== reqParamType) {
                                    if (fnParamType === "boolean") {
                                        reqParamValue = reqParamValue === "true";
                                    } else if (fnParamType === "number") {
                                        reqParamValue = parseFloat(reqParamValue);
                                        if (isNaN(reqParamValue)) {
                                            reqParamValue = undefined;
                                        }
                                    } else if (fnParamType === "string") {
                                        reqParamValue = reqParamValue;
                                    } else if ((paramName === "req" || paramName === "request") && fnParamType === "object") {
                                        reqParamValue = request;
                                    }
                                }
                                args.push(reqParamValue);
                            }

                            // Call the method and get the result
                            const result = await classInstance[methodName].apply(classInstance, args);

                            // Send result to client
                            if (ws.readyState === ws.OPEN) {
                                const r = {
                                    data : result,
                                    id: messageId,
                                    type: messageType,
                                };
                                if(ExpressWebsocketGenerator.debugLogging){
                                    console.log("Sending result:",r);
                                }
                                ws.send(JSON.stringify(r));
                            }else{
                                if(ExpressWebsocketGenerator.debugLogging){
                                    console.log("WS readystate != OPEN, not sending result! State:", ws.readyState);
                                }
                            }
                        } catch (e) {
                            if (ws.readyState === ws.OPEN) {
                                const r = {
                                    error : e.toString(),
                                    id: messageId,
                                    type: messageType,
                                };
                                if(ExpressWebsocketGenerator.debugLogging){
                                    console.log("Error", e , " Sending response:", r);
                                }
                                ws.send(JSON.stringify(r));
                            }
                        }

                    };
                }
            }
        }
        return router;
    }

    /**
     * Adds a listener for event and returns that listener function so that it can later be removed
     * @param classInstances
     * @param eventPath
     * @param ws
     */
    private static addWebsocketListener(classInstances: any[], eventPath: string, ws: any): any {
        const targetClassName = eventPath.split(".")[0];
        const targetEventName = eventPath.split(".")[1];

        for (const classInstance of classInstances) {
            const className = classInstance.constructor.name;

            if (targetClassName !== className) {
                continue;
            }

            for (const methodName in classInstance) {
                if (targetEventName !== methodName) {
                    continue;
                }
                if (typeof(classInstance[methodName].addListener) === "function") { // Handle event emitters
                    const listener = (data: any) => {
                        if(ws.readyState === ws.OPEN){
                            ws.send(JSON.stringify({
                                type: "event:" + className + "." + methodName,
                                data,
                            }));
                        }
                    };
                    classInstance[methodName].addListener(listener);
                    return listener;
                } else {
                    console.log("Cannot add event listener for path:", eventPath, " because target element is not event");
                }
            }
        }
    }

    /**
     * Removes listener from certain event
     * @param classInstances
     * @param eventPath
     * @param listener
     */
    private static removeWebsocketListener(classInstances: any[], eventPath: string, listener: any): boolean {
        const targetClassName = eventPath.split(".")[0];
        const targetEventName = eventPath.split(".")[1];

        for (const classInstance of classInstances) {
            const className = classInstance.constructor.name;
            if (targetClassName !== className) {
                continue;
            }

            for (const methodName in classInstance) {
                if (targetEventName !== methodName) {
                    continue;
                }

                if (typeof(classInstance[methodName].removeListener) === "function") { // Handle event emitters
                    classInstance[methodName].removeListener(listener);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Removes listeners from all events
     * @param classInstances
     * @param listeners
     */
    private static removeAllWebsocketListeners(classInstances: any[], listeners: any[]): boolean {
        for (const classInstance of classInstances) {
            for (const methodName in classInstance) {
                if (typeof(classInstance[methodName].removeAllListeners) === "function") { // Handle event emitters
                    for (const listener of listeners) {
                        classInstance[methodName].removeListener(listener);
                    }
                    return true;
                }
            }
        }
        return false;
    }


    // Argument in this case is usually function
    private static getStringTypeFromReflectionType(type: any): string {
        return type.name.replace("String", "string").replace("Number", "number").replace("Boolean", "boolean").replace("Object", "object").replace("Array", "any[]");
    }

    private static lintCode(fileName: string): string {
        const options: ILinterOptions = {
            fix: true,
            formatter: "json",
            formattersDirectory: "customFormatters/",
            rulesDirectory: "customRules/",
        };

        const fileContents = fs.readFileSync(fileName, "utf8");
        const linter = new Linter(options);
        const configuration = Configuration.findConfiguration(undefined, fileName).results;
        linter.lint(fileName, fileContents, configuration);
        const result = linter.getResult();
        return result.output;
    }
}
