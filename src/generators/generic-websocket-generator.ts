import * as fs from "fs";
import * as path from "path";
import * as ts from "typescript";
import { IDictionary } from "../interfaces/dictionary";
import { Utils } from "../utils";
import { IClientGeneratorSettings } from "./client-generator-settings";
import { IWebsocketGenerator } from "./websocket-generator";


export class GenericWebsocketGenerator implements IWebsocketGenerator {

    private static getEventEmitterClassCode(): string {
        const currentDir = path.dirname(__filename);
        const eeFilePath = path.normalize(path.join(currentDir, "../../src/utils/websocket-event-emitter.ts"));
        return fs.readFileSync(eeFilePath).toString().replace("export ", "");
    }

    /**
     * Generates a class that can be used on a frontend/client for retriving requests from REST server.
     * @param tsClassFilePath path to typescript class that contains class with @WebsocketClass and @WebsocketMethod annotations
     * @param className name of class that we would like to expose
     * @param outputFile  path to output file where we would like to save the generated service class
     * @param embedInterfaces if false then external classes/interfaces will be imported and if true then they will be inlined
     */
    public generateClientServiceFromFile(tsClassFilePath: string, outputFile: string, settings?: IClientGeneratorSettings): string {
        if (!fs.existsSync(tsClassFilePath)) {
            throw new Error("Cannot find TS file from path:" + tsClassFilePath);
        }

        const sourceCode = fs.readFileSync(tsClassFilePath, "utf-8");
        const sourceFile = ts.createSourceFile(tsClassFilePath, sourceCode, ts.ScriptTarget.Latest, true);

        // Check if AST was generated and if not then throw error
        if (!sourceFile) {
            throw new Error(`Error parsing typescript source file ${tsClassFilePath} !`);
        }

        // Find class declaration of type Class declaration that has decorators and that its name is equal to classes name that we require
        const classDeclaration = sourceFile.statements.filter((x: ts.Statement) => {
            return x.kind === ts.SyntaxKind.ClassDeclaration &&
                    ( (settings && (x as any).name.getText() === settings.className) ||
                      (!settings || !settings.className)
                    );
                })[0] as any as ts.ClassDeclaration;

        const apiPrefix = classDeclaration.name.getText();
        const hostname = settings.hostname ? settings.hostname : "window.location.hostname";
        let host = "window.location.host";
        if(settings.hostname && settings.port){
            host = `"${settings.hostname}:${settings.port}"`;
        }else if(settings.port){
            host = `window.location.hostname + ":${settings.port}"`; //If port is specified then use hostname+port otherwise use window.host
        }
        const outputDir = path.dirname(outputFile);
        const importFiles: IDictionary<string[]> = {};
        const importFilesAbsolutePaths: string[] = [];
        let clientServiceClass = "";

        if (classDeclaration) {
            const className = classDeclaration.name!!.getText();
            clientServiceClass += "/* tslint:disable*/";
            // clientServiceClass += "var __wsInstance__: WebSocket | undefined = undefined;\n";
            // clientServiceClass += "declare global var __wsConnectionPromise__: Promise<WebSocket> | undefined;\n\n";
            clientServiceClass += GenericWebsocketGenerator.getEventEmitterClassCode();
            clientServiceClass += "export class " + className + "Service {\n\n";
            clientServiceClass += "\tprivate className = \"" + className + "\";\n";
            clientServiceClass += `\tprivate serverUrl = "ws://" + ${host} + "/ws";\n\n`;
            clientServiceClass += this.generateClassHeader(className);

            for (const m in classDeclaration.members) {
                const member = classDeclaration.members[m];
                const restDecorators = member.decorators !== undefined && member.decorators.length > 0 ? member.decorators.filter((x) => x.getText().includes("WebsocketMethod"))  :  [];
                if (member.kind === ts.SyntaxKind.MethodDeclaration && restDecorators.length > 0) {
                    const methodDeclaration = member as ts.MethodDeclaration;
                    const methodName = methodDeclaration.name.getText();

                    // Collect argument names and types
                    const argumentNames = methodDeclaration.parameters.map((x) => x.name.getText());
                    const argumentTypes = methodDeclaration.parameters.map((x) => x.type ? x.type.getText() : "any");
                    const methodType = Utils.getRequestMethodType(methodName, argumentTypes);

                    // Exclude Request parameters from method if they exist (they will be injected but they should not be passed via request)
                    let requestArgumentIndex = argumentTypes.indexOf("Request");
                    while (requestArgumentIndex >= 0) {
                        argumentNames.splice(requestArgumentIndex, 1);
                        argumentTypes.splice(requestArgumentIndex, 1);
                        requestArgumentIndex = argumentTypes.indexOf("Request");
                    }

                    // Check if input parameter is anything other than basic type
                    const nonPrimitiveTypes = argumentTypes.filter((x) => /(string|number|boolean|any|object)/.test(x) === false);
                    if (nonPrimitiveTypes.length > 0) {
                        throw new Error(`ERROR, detected non primitive type in ${classDeclaration.name!!.getText()}.${methodName}!`);
                    }

                    // Get return type (excluding Promise< and >)
                    let returnType = methodDeclaration.type ?  methodDeclaration.type!!.getText() : "";
                    returnType = returnType.indexOf("Promise<") === 0 ? returnType.replace(/(Promise<?)|>$/g, "") : returnType;
                    this.addTypeToImportFiles(returnType, sourceFile, outputDir, importFiles, importFilesAbsolutePaths);

                    const optionalArgs = methodDeclaration.parameters.filter((x) => {
                            return x.questionToken || x.initializer || x.dotDotDotToken;
                        }).map((x) => {
                            return x.name.getText();
                    });

                    // Create method
                    const methodString = this.generateClientMethodDefinition(methodName, methodType, Utils.getEndpointPath(methodName, apiPrefix), argumentNames, argumentTypes, optionalArgs, returnType);
                    clientServiceClass += methodString;
                    clientServiceClass += "\n";
                } else if (member.kind === ts.SyntaxKind.PropertyDeclaration && (member as ts.PropertyDeclaration).type && (member as ts.PropertyDeclaration).type.getText().indexOf("WebsocketEventEmitter") === 0) { // Event emitters
                    clientServiceClass += "\n";
                    clientServiceClass += member.getText();
                    clientServiceClass += "\n";

                    const prop = (member as ts.PropertyDeclaration);

                    // Check for generic type argument and if it exists and get its type if it is not a standard type
                    const returnType = prop.type.getText().replace(/(WebsocketEventEmitter<?)|>$/g, "");
                    this.addTypeToImportFiles(returnType, sourceFile, outputDir, importFiles, importFilesAbsolutePaths);

                    // Ensure that constructor for EventEmitter is called when property is created
                }
            }
        }
        clientServiceClass += "}";

        // Add import statements or embed imported classes as interfaces
        if (settings && settings.embedInterfaces) {
            clientServiceClass = this.generateInterfacesForImportFiles(importFilesAbsolutePaths).interfaceDefinitions + clientServiceClass;
        } else {
            clientServiceClass = this.formatImportStatements(importFiles) + clientServiceClass;
        }

        return clientServiceClass;
    }


    /**
     * Generates common things that are used inside class
     */
    protected generateClassHeader(className: string): string {

        const method =
    `
    private ws?: WebSocket;
    private successHandlers: { [Key: string]: any} = {};
    private errorHandlers: { [Key: string]: any} = {};

    private uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    private async getGlobalWSConnection(): Promise<WebSocket>{
        if (!(window as any).__wsInstance__) {
            if((window as any).__wsConnectionPromise__){
				return (window as any).__wsConnectionPromise__;
            }
            (window as any).__wsConnectionPromise__ = new Promise<WebSocket>((resolve, reject)=>{
                const socket = new WebSocket(this.serverUrl);
                socket.addEventListener("open", () => {
                    (window as any).__wsInstance__ = socket;
                    (window as any).__wsConnectionPromise__ = undefined;
                    resolve((window as any).__wsInstance__);
                });

                socket.addEventListener("close", () => {
                    (window as any).__wsInstance__ = undefined;
                    (window as any).__wsConnectionPromise__ = undefined;
                });

                socket.addEventListener("error",(e) => {
                    console.error("Websocket error:", e);
                    reject(e);
                });
            })
            return (window as any).__wsConnectionPromise__;
        } else {
            return (window as any).__wsInstance__;
        }
    }

    private async getConnection(): Promise<WebSocket>{
        if(!this.ws){
            this.ws = await this.getGlobalWSConnection();
            this.ws.addEventListener("close",()=>{
                this.ws = undefined;
            });
            this.ws.addEventListener("message",this.handleResponse.bind(this));
        }
        return this.ws;
        
    }

    public async handleResponse(message: any): Promise<any> {
        try{
            let json = JSON.parse(message.data);
            if (json.error) {
                if (this.errorHandlers[json.id]) {
                    this.errorHandlers[json.id](json.error);
                }
            } else if (json.type.indexOf("event:"+this.className) == 0) {
                //Handle events
                const eventEmitterName = json.type.replace("event:" + this.className + ".","");
                if(this[eventEmitterName]){
                    this[eventEmitterName].emit(json.data);
                }else{
                    console.error("Event emitter:", eventEmitterName, "does not exist");
                }
            } else if (this.successHandlers[json.id] !== undefined) {
                this.successHandlers[json.id](json.data);
            }
            if(message.id){
                this.successHandlers[message.id] = undefined;
                this.errorHandlers[message.id] = undefined;
            }
        }catch(e){
            console.error("Error handling websocket message", message.data, "Exception:",   e);
        }
    }

    public async callMethod(method: string, data?: any): Promise<any> {
        return new Promise(async (resolve, reject) => {
            const ws = await this.getConnection();
            const message = {
                id : this.uuidv4(),
                type : this.className + "." + method,
                data : data,
            };
            this.successHandlers[message.id] = resolve;
            this.errorHandlers[message.id] = reject;
            ws.send(JSON.stringify(message));
        });
    }

    constructor(){
        this._init();
    }

    private async _init(){
        //For every event add callback for first and last subscriber
        for(const propertyName in this){
            const property = this[propertyName] as any;
            if(!(property instanceof WebsocketEventEmitter)){
                continue;
            }

            property.onEventAdded = async ()=>{
                if(property.listenerCount() == 1){
                    property.onEventAdded = undefined;

                    (await this.getConnection()).send(JSON.stringify({
                        type : "event-subscribe:${className}" + "." + propertyName,
                    }));
                }
            }

            property.onEventRemoved = async ()=>{
                if(property.listenerCount() == 0){
                    (await this.getConnection()).send(JSON.stringify({
                        type : "event-unsubscribe:${className}" + "." + propertyName,
                    }));
                }
            }
        }
    }
    `;

        return method;
    }

    /**
     * Goes through import files and creates interfaces
     */
    protected generateInterfacesForImportFiles(importFiles: string[]): { interfaceDefinitions: string, interfaceNames: string[]} {
        let interfaceDefinitions = "";
        let generatedInterfaceNames: string[] = [];

        for (const filename of importFiles) {
            const filenameString = fs.readFileSync(filename, "utf-8");
            const sourceFile = ts.createSourceFile(filename, filenameString, ts.ScriptTarget.Latest, true);

            // Go through all classes and interfaces and generate them
            for (const statement of sourceFile.statements) {
                if (statement.kind === ts.SyntaxKind.ClassDeclaration || statement.kind === ts.SyntaxKind.InterfaceDeclaration || statement.kind === ts.SyntaxKind.EnumDeclaration) {
                    const generatedInterfaces = this.generateInterface(statement as any, true, generatedInterfaceNames);
                    if (generatedInterfaces) {
                        interfaceDefinitions += generatedInterfaces.interfaceDeclarations;
                        interfaceDefinitions += "\n\n";

                        generatedInterfaceNames = generatedInterfaceNames.concat(generatedInterfaces.interfaceNames);

                        // Check what kind of properties does the interface contains
                        const allPropertyTypes = this.getPropertyTypes(statement as any);
                        for (const propertyType of allPropertyTypes) {
                            // skip system data types
                            if (/(string|object|number|any|boolean|number|undefined)/.test(propertyType)) {
                                continue;
                            }

                            // Check if we already have interface for type and if not try to resolve it
                            if (generatedInterfaceNames.indexOf(propertyType) < 0) {
                                const declaration = this.getDeclarationFor(propertyType, sourceFile);
                                this.generateInterface;
                                generatedInterfaceNames.push();
                            }
                        }
                    }
                }
            }
        }
        return { interfaceDefinitions, interfaceNames: generatedInterfaceNames };
    }

    protected getFilePathFromImportDeclaration(importDeclaration: ts.ImportDeclaration, importDeclarationFilePath: string): string | undefined {
        const originalRelativeImportPathRegExResult = /\"(.|\.|\/)*\"/.exec(importDeclaration.getText());
        if (originalRelativeImportPathRegExResult && originalRelativeImportPathRegExResult.length > 0 && originalRelativeImportPathRegExResult[0].length > 0) {
            const originalRelativeImportPath = originalRelativeImportPathRegExResult[0].replace(/(\"|\')/g, "");

            // Covert relative path from original file to path for new output file
            const fullImportPath = path.normalize(path.join(path.dirname(importDeclarationFilePath), originalRelativeImportPath)).replace(/\\/g, "/");
            const fullPath = `./${fullImportPath}.ts`;
            return fullPath;
        } else {
            return undefined;
        }

    }

    /**
     * Generates interfaces for specified declaration (also includes child interface declarations). Returns array of generated interface names and stirng that contains all interface definitions
     * @param declaration
     * @param recursive
     * @param ignoreDeclarations
     */
    protected generateInterface(declaration: ts.ClassDeclaration | ts.InterfaceDeclaration | ts.EnumDeclaration, recursive: boolean, ignoreDeclarations: string[]): { interfaceDeclarations: string, interfaceNames: string[] } {
        const out = {
            interfaceDeclarations : "",
            interfaceNames : [] as string[],
        };

        if (!declaration.name || ignoreDeclarations.indexOf(declaration.name!!.getText()) >= 0) {
            return out;
        }

        if (recursive) {
            // Check what kind of properties does the interface contains
            const allPropertyTypes = this.getPropertyTypes(declaration as any);
            for (const propertyType of allPropertyTypes) {
                // Skip system data types, ignored types and already added types
                if (/(string|object|number|any|boolean|number|undefined)/.test(propertyType) || ignoreDeclarations.indexOf(propertyType) >= 0 || out.interfaceNames.indexOf(propertyType) >= 0) {
                    continue;
                }

                const sourceFile = (declaration as ts.Node).getSourceFile();
                // Check if we already have interface for type and if not try to resolve it
                const childDeclaration = this.getDeclarationFor(propertyType, sourceFile);
                if (childDeclaration) {
                    const childrenInterfaces  = this.generateInterface(childDeclaration as any, recursive, ignoreDeclarations.concat(out.interfaceNames));

                    if (childrenInterfaces.interfaceNames.length > 0) {
                        out.interfaceDeclarations += childrenInterfaces.interfaceDeclarations + "\n\n";
                        out.interfaceNames = out.interfaceNames.concat(childrenInterfaces.interfaceNames);
                    }
                }
            }
        }

        if (declaration.kind === ts.SyntaxKind.ClassDeclaration) {
            const interfaceDeclaration = (declaration as ts.ClassDeclaration);
            const interfaceName = interfaceDeclaration.name!!.getText();

            let interfaceDef = "interface " + interfaceName + " {\n";

            // Go through all properties and output them in output string
            for (const member of interfaceDeclaration.members) {
                if (member.kind === ts.SyntaxKind.PropertyDeclaration) {
                    const propertyName = (member as ts.PropertyDeclaration).name.getText();
                    const propertyType = (member as any).type ? (member as ts.PropertyDeclaration).type!!.getText() : undefined;

                    if (propertyName && propertyType) {
                        interfaceDef += `\t${propertyName}: ${propertyType};\n`;
                    }
                }
            }

            interfaceDef += "}\n\n";

            out.interfaceDeclarations += interfaceDef;
            out.interfaceNames.push(interfaceName);

        } else if (declaration.kind === ts.SyntaxKind.InterfaceDeclaration) {
            out.interfaceDeclarations += declaration.getText();
            out.interfaceNames.push(declaration.name.getText());
        } else if ((declaration as any).kind === ts.SyntaxKind.EnumDeclaration) {
            out.interfaceDeclarations += (declaration as ts.EnumDeclaration).getText();
            out.interfaceNames.push((declaration as ts.EnumDeclaration).name.getText());
        }
        return out;
    }

    protected getPropertyTypes(declaration: ts.ClassDeclaration | ts.InterfaceDeclaration): string[] {
        const types: string[] = [];

        for (const member of declaration.members) {
            if (member.kind === ts.SyntaxKind.PropertySignature || member.kind === ts.SyntaxKind.PropertyDeclaration) {
                const type = (member as ts.PropertyDeclaration).type;
                const t = type ? type!!.getText() : undefined;
                if (t && types.indexOf(t) < 0) {
                    types.push(t);
                }
            }
        }
        return this.sanitizeTypes(types);
    }

    protected sanitizeTypes(types: string[]): string[] {
        const outTypes: string[] = [];
        const newTypes = types.map((x) => x.replace(/(\[\]|<|>)/g, " "));

        for (const item of newTypes) {
            const splitItems = item.split(" ");
            for (const si of splitItems) {
                if (si.length > 0 && outTypes.indexOf(si) < 0) {
                    outTypes.push(si);
                }
            }
        }
        return outTypes;
    }

    protected getDeclarationFor(declarationName: string, file: ts.SourceFile): ts.DeclarationStatement | undefined {
        // Go through all classes and interfaces and try to find the declaration
        for (const statement of file.statements) {
            if ((statement.kind === ts.SyntaxKind.ClassDeclaration || statement.kind === ts.SyntaxKind.InterfaceDeclaration || statement.kind === ts.SyntaxKind.EnumDeclaration) && (statement as any).name && (statement as any).name!!.getText() === declarationName) {
                return statement as ts.DeclarationStatement;
            } else if (statement.kind === ts.SyntaxKind.ImportDeclaration) {
                const importDeclaration = statement as ts.ImportDeclaration;
                // try to search for declaration in a imported file file
                if (importDeclaration.importClause && importDeclaration.importClause!!.getText().indexOf(declarationName) > 0) {
                    const importPath = this.getFilePathFromImportDeclaration(importDeclaration, file.fileName);
                    if (importPath) {
                        const filenameString = fs.readFileSync(importPath, "utf-8");
                        const sourceFile = ts.createSourceFile(importPath, filenameString, ts.ScriptTarget.Latest, true);
                        const declaration = this.getDeclarationFor(declarationName, sourceFile);
                        if (declaration) {
                            return declaration;
                        }
                    }
                }
            }
        }
        return undefined;
    }

    /**
     * Converts dictionary that contains file paths as keys and array of class/interface
     * names as values into typescript import statements
     */
    protected formatImportStatements(importFiles: IDictionary<string[]>): string {
        // Generate import statements
        let importStatements = "";
        for (const file in importFiles) {
            importStatements += this.generateImportStatement(importFiles[file], file);
            importStatements += "\n";
        }
        return importStatements;
    }

    protected generateImportStatement(classNames: string[], sourceFilePath: string): string {
        return `import { ${classNames.join(", ")} } from "${sourceFilePath.replace(/\\/g, "/")}";`;
    }

    protected generateClientMethodDefinition(methodName: string, methodType: string, urlEnpoint: string, argumentNames: string[], argumentTypes: string[], optionalArgs: string[], returnType: string): string {
        if (argumentNames.length !== argumentTypes.length) {
            throw new Error("Length of method types and argument names does not match!");
        }

        let methodArgs = "";
        let params = "";

        for (let i = 0; i < argumentNames.length; i++) {
            const posix = optionalArgs.indexOf(argumentNames[i]) >= 0 ? "?" : "";

            methodArgs += argumentNames[i] + posix + ": " + argumentTypes[i];
            if (i !== argumentNames.length - 1) {
                methodArgs += ", ";
            }

            if (params.length === 0) {
                params += "{";
            }
            params += argumentNames[i] + ": " + argumentNames[i] + ",";
        }

        if (params.length > 0) {
            params += "}";
        } else {
            params = "undefined";
        }

        // If method returns a value then set return type to a specified return type
        if (returnType.length > 0) {
            returnType = ": Promise<" + returnType + ">";
        } else {
            returnType = ": Promise<void>";
        }
        const methodString = `
    public async ${methodName}(${methodArgs}) ${returnType}{
        return this.callMethod(\"${methodName}\",${params});
    }`;

        return methodString;
    }

    private addTypeToImportFiles(returnType: string, sourceFile: ts.SourceFile, outputDir: string, importFiles: IDictionary<string[]>, importFilesAbsolutePaths: string[]) {
        // Get return type (excluding Promise< and >)
        // let returnType = methodDeclaration.type ?  methodDeclaration.type!!.getText() : "";
        const allReturnTypes = returnType.replace(/(<|>|\[|\])/g, " ").split(" ").map((x) => x.trim()) .filter((x) => x.length > 0);

        // Find import statement for return type (returm type can have nested types so resolve those)
        for (const rt of allReturnTypes) {
            const importDeclaration = sourceFile.statements.filter((x) => (x as any).importClause && (x as any).importClause.getText().indexOf(rt) >= 0)[0] as ts.ImportDeclaration;
            if (importDeclaration && returnType.length > 0) {
                const originalRelativeImportPathRegExResult = /\"(.|\.|\/)*\"/.exec(importDeclaration.getText());
                if (originalRelativeImportPathRegExResult && originalRelativeImportPathRegExResult.length > 0 && originalRelativeImportPathRegExResult[0].length > 0) {
                    const originalRelativeImportPath = originalRelativeImportPathRegExResult[0].replace(/(\"|\')/g, "");

                    // Covert relative path from original file to path for new output file
                    const fullImportPath = path.normalize(path.join(path.dirname(sourceFile.fileName), originalRelativeImportPath)).replace(/\\/g, "/");
                    const fullPath = `./${fullImportPath}.ts`;

                    const importPath = path.normalize(path.relative(outputDir, fullImportPath));
                    // console.log("import path", importPath, "full path", fullImportPath);
                    if (!importFiles[importPath]) {
                        importFiles[importPath] = [rt];
                        importFilesAbsolutePaths.push(fullPath);
                    } else if (importFiles[importPath].indexOf(rt) < 0) {
                        importFiles[importPath].push(rt);
                        importFilesAbsolutePaths.push(fullPath);
                    }
                }
            }
        }
    }
}
