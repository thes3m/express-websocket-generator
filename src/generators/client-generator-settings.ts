export interface IClientGeneratorSettings {
    embedInterfaces?: boolean;
    className?: string;
    port?: number;
    hostname?: string;
}
