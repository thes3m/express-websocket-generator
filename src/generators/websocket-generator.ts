import { IClientGeneratorSettings } from "./client-generator-settings";
export interface IWebsocketGenerator {
    generateClientServiceFromFile(tsClassFilePath: string, outputFile: string, settings?: IClientGeneratorSettings): string;
}
