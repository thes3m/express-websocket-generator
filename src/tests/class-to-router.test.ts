import { expect } from "chai";
import { Router } from "express";
import * as express from "express";

import * as http from "http";
import * as WebSocket from "ws";
import { ExpressWebsocketGenerator } from "../express-ts-websocket-generator";
import { RestClass, RestClassWithExposedMethods, RestClassWithoutExposedMethods } from "./test-rest-classes";

const port = 3000;
const app = express();
// you explicitly create the http server
const httpServer = http.createServer(app);


// if you used the '@types/mocha' method to install mocha type definitions, uncomment the following line
// import 'mocha';

describe("Server Express router generation", () => {
  const getRouterPaths = (router: Router) => {
    return router.stack.filter((r) => r.route).map((r) => r.route.path);
  };

  it("should connect to websocket server", (done) => {
    const classInstance = new RestClassWithExposedMethods();
    ExpressWebsocketGenerator.generateWebsocketServer(app, [classInstance]);
    app.listen(port, () => {
      // Connect to websocket and then test if methods exist
      const socket = new WebSocket(`ws://localhost:${port}/`);
      socket.onopen = () => {
        expect(true, "Client connected to websocket server");
        done();
      };

      socket.onerror = (e) => {
        console.error(e.toString());
        done();
      };

      socket.onmessage = (message: any) => {
        console.log("Received message:", message);
        // done();
      };
    });
  });

  // it("should generate websocket client", () => {
  //   const classInstance = new RestClassWithExposedMethods();


  //   expect(getRouterPaths(router)).to.eql([]);
  // });

  // it("should create an endpoint with 2 methods", () => {
  //   const classInstance = new RestClassWithExposedMethods();
  //   const router = ExpressRESTGenerator.generateWebsocketServer(server, [classInstance]);

  //   //
  //   expect(getRouterPaths(router)).to.eql(["/get-data", "/store-item-optional"]);
  // });
});
