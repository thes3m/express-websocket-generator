import { WebsocketEventEmitter } from "..";
import { WebsocketClass } from "../decorators/rest-api";
import { WebsocketMethod } from "../decorators/rest-method";


export class RestClass {
    public data = ["test", "test2", "test3"];

    public getData(count: number): any[] {
        return this.data;
    }
}

@WebsocketClass("")
export class RestClassWithoutExposedMethods {
    public data = ["test", "test2", "test3"];

    
    public getData(count: number): any[] {
        return this.data;
    }
}

export interface ITestData {
    data: number;
    timestamp: string;
}

@WebsocketClass("")
export class RestClassWithExposedMethods {
    public data = ["test", "test2", "test3"];

    public event: WebsocketEventEmitter<ITestData> = new WebsocketEventEmitter<ITestData>();

    @WebsocketMethod()
    public getData(count?: number): any[] {
        return this.data.slice(0, count);
    }

    @WebsocketMethod()
    public storeItemOptional(item?: string, defaultValue: string = "test"): boolean {
        this.data.push(item);
        return true;
    }

    public nonRestMethod(): boolean {
        return false;
    }
}
