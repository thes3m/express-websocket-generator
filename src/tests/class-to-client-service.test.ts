
import { expect } from "chai";
import * as fs from "fs";
import { ExpressWebsocketGenerator } from "../express-ts-websocket-generator";

describe("Client class generation tests", () => {
    const outFile = "./generated/RestClassService.ts";

    it("should not generate client websocket code", () => {
        expect(() => {
            ExpressWebsocketGenerator.generateClientServiceFromFile(__dirname + "/test.ts", outFile);
        }).to.throw();
    });

    it("should generate client websocket code", () => {
        ExpressWebsocketGenerator.generateClientServiceFromFile(__dirname + "/test-rest-classes.ts", outFile, "default", {
            className : "RestClassWithExposedMethods",
            embedInterfaces : true,
        });
        const exists = fs.existsSync(outFile);
        expect(exists).to.be.true;
    });

    // after(() => {
    //     if (fs.existsSync(outFile)) {
    //         fs.unlinkSync(outFile);
    //     }
    // });
});
