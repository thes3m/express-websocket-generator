import "reflect-metadata";

/**
 * Decorated a class method to be exported as a rest method.
 * @param target
 * @param key
 * @param descriptor
 */
export function WebsocketMethod() {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        //console.log("Rest method decorator:", target,propertyKey, descriptor);
        return descriptor;
    };
  }

