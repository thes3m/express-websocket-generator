export class WebsocketEventEmitter<T = void> {
    public onEventAdded: () => void;
    public onEventRemoved: () => void;

    private callbacks: any[] = [];

    public addListener(callback: (data: T) => void) {
        this.callbacks.push(callback);
        if (this.onEventAdded) {
            this.onEventAdded();
        }
    }

    public emit(data: T) {
        for (const cb of this.callbacks) {
            cb(data);
        }
    }

    public removeListener(callback: (data: T) => void) {
        if (this.callbacks.indexOf(callback) >= 0) {
            this.callbacks.splice(this.callbacks.indexOf(callback), 1);
        }
        if (this.onEventRemoved) {
            this.onEventRemoved();
        }
    }

    public removeAllListeners() {
        this.callbacks.splice(0, this.callbacks.length);
        if (this.onEventRemoved) {
            this.onEventRemoved();
        }
    }

    public listenerCount() {
        return this.callbacks.length;
    }
}
